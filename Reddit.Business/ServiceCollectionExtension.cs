﻿using Microsoft.Extensions.DependencyInjection;
using Reddit.Business.ChatManagement;
using Reddit.Business.CommentManagement;
using Reddit.Business.CommunityManagment;
using Reddit.Business.PostManagement;
using Reddit.Business.ReduxLoaderManagement;
using Reddit.Business.UserManagement;

namespace Reddit.Business
{
    public static class ServiceCollectionExtension
    {
        public static void AddBussinesServices(this IServiceCollection services)
        {
            services.AddScoped<IReduxLoaderManager, ReduxLoaderManager>();
            services.AddScoped<IPostManager, PostManager>();
            services.AddScoped<ICommentManager, CommentManager>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<ICommunityManager, CommunityManager>();
            services.AddScoped<IChatManager, ChatManager>();
        }
    }
}
