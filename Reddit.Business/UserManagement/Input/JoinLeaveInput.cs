﻿namespace Reddit.Business.UserManagement.Input
{
    public class JoinLeaveInput
    {
        public long CommunityId { get; set; }
        public long UserId { get; set; }
    }
}
