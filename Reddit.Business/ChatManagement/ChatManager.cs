﻿using Microsoft.AspNetCore.SignalR;
using Reddit.Domain.Entities;
using Reddit.Domain.Entities.Enums;
using Reddit.Domain.Interop;
using Reddit.Services.Hubs;
using Reddit.Services.Repositories;
using Reddit.Services.Repositories.Chat;
using Reddit.Services.Repositories.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reddit.Business.ChatManagement
{
    public class ChatManager : IChatManager
    {
        private readonly IRedisRepository _redisRepository;
        private readonly IChatRepository _chatRepository;
        private readonly IUserRepository _userRepository;
        private readonly IHubContext<MessageHub> _hubContext;

        public ChatManager(
            IRedisRepository redisRepository,
            IChatRepository chatRepository,
            IUserRepository userRepository,
            IHubContext<MessageHub> hubContext)
        {
            _redisRepository = redisRepository;
            _chatRepository = chatRepository;
            _userRepository = userRepository;
            _hubContext = hubContext;
        }

        public async Task<Result<LoadChatDataOutput>> LoadChatDataAndSubscribe(SubscribeInput input)
        {
            if (input == null) return new Result<LoadChatDataOutput>() { Success = false };

            input.Communities.ForEach(async community =>
            {
                await _hubContext.Groups.AddToGroupAsync(
                    input.ConnectionId, community.ToString()).ConfigureAwait(false);
            });


            List<Message> messages = await _chatRepository.
                FindFrom(input.Communities).ConfigureAwait(false);

            List<User> users = await _userRepository.FindManyUsersAsync(
                messages.ConvertAll(message => message.Sender)).ConfigureAwait(false);

            return new Result<LoadChatDataOutput>()
            {
                Success = true,
                Data = new LoadChatDataOutput()
                {
                    Messages = messages,
                    Users = users
                }
            };
        }

        public async Task<Result<long>> SendMessage(Message message)
        {
            long id = await _chatRepository.CreateAsync(message).ConfigureAwait(false);

            await _hubContext.Clients
                .Group(message.Community.ToString())
                .SendAsync("SendMessage", message)
                .ConfigureAwait(false);

            List<Community> communities = await _redisRepository
                .GetAsync<List<Community>>(RedisKeys.COMMUNITIES)
                .ConfigureAwait(false);

            Community community = communities
                .Find(community => community.Id == message.Community);

            community.Messages.Add(id);

            await _redisRepository.SetAsync(RedisKeys.COMMUNITIES, communities)
                .ConfigureAwait(false);

            if (id == 0) return new Result<long>() { Success = false };

            return new Result<long>()
            {
                Success = true,
                Data = id
            };
        }

        public async Task<Result> Subscribe(SubscribeInput input)
        {
            if (input == null) return new Result() { Success = false };

            input.Communities.ForEach(async community =>
            {
                await _redisRepository.Subscribe(community.ToString(), input.ConnectionId)
                    .ConfigureAwait(false);

            });

            return new Result() { Success = true };
        }
    }
}
