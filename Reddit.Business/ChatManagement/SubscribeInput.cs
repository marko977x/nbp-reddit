﻿using System.Collections.Generic;

namespace Reddit.Business.ChatManagement
{
    public class SubscribeInput
    {
        public string ConnectionId { get; set; }
        public List<long> Communities { get; set; }
    }
}
