﻿using Reddit.Domain.Entities;
using System.Threading.Tasks;

namespace Reddit.Services.Repositories.Communities
{
    public interface ICommunityRepository : INeo4JRepository<Community>
    {
        Task UpdateCommunityAsync(Community community);
    }
}
