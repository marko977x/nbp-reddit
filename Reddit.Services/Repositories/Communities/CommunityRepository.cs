﻿using Reddit.Domain.Entities;
using Reddit.Neo4J;
using System.Linq;
using System.Threading.Tasks;

namespace Reddit.Services.Repositories.Communities
{
    public class CommunityRepository : Neo4JRepository<Community>, ICommunityRepository
    {
        public CommunityRepository(IGraphDatabase graphDatabase) : base(graphDatabase) { }

        protected override string NodeLabel => "Community";

        public override async Task<long> CreateAsync(Community entity)
        {
            await Graph.Cypher
                .Merge("(node:" + NodeLabel + " {" + GetCommunityData(entity) + "})")
                .ExecuteWithoutResultsAsync();

            return entity.Id;
        }

        private string GetCommunityData(Community entity)
        {
            return $"Id: {entity.Id}, Title: \"{entity.Title}\"";
        }

        public override async Task<Community> FindAsync(long id)
        {
            Community community = Graph.Cypher
                .Match("(community:Community)")
                .Where((Community community) => community.Id == id)
                .OptionalMatch("(community:Community)-[:HAS_USER]->(users:User)")
                .OptionalMatch("(community:Community)-[:HAS_POST]->(posts:Post)")
                .OptionalMatch("(community:Community)-[:HAS_MESSAGE]->(message:Message)")
                .Return((community, users, posts, message) =>
                    new
                    {
                        community.As<Community>().Id,
                        community.As<Community>().Title,
                        Users = users.CollectAsDistinct<User>(),
                        Posts = posts.CollectAsDistinct<Post>(),
                        Messages = message.CollectAsDistinct<Message>()
                    }
                )
                .Results.Select(myObject => new Community()
                {
                    Id = myObject.Id,
                    Title = myObject.Title,
                    Users = myObject.Users.ToList().ConvertAll(user => user.Id).ToList(),
                    Posts = myObject.Posts.ToList().ConvertAll(post => post.Id).ToList(),
                    Messages = myObject.Messages.ToList().ConvertAll(message => message.Id).ToList()
                })
                .FirstOrDefault();

            return community;
        }

        public async Task UpdateCommunityAsync(Community community)
        {
            await Graph.Cypher.Match($"(comm:{NodeLabel})")
                 .Where((Community comm) => comm.Id == community.Id)
                 .Set("comm = {entity}")
                 .WithParam("entity", community)
                 .ExecuteWithoutResultsAsync();
        }
    }
}
