﻿using StackExchange.Redis;

namespace Reddit.Redis
{
    public interface IRedisDatabase
    {
        IDatabase RedisDB { get; }
        ISubscriber PubSub { get; }
    }
}
