import { Button, FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Header from '../components/header/Header';
import Post from '../components/post/Post';
import { NormalizedObjects } from '../store';
import { initLoadMoreCommunityPosts } from '../store/post/action';
import { PostState } from '../store/post/types';
import { initSortCommunityPosts, startSpinner } from '../store/ui/action';
import { UiState } from '../store/ui/types';
import styles from "./css/home.module.css";
import { RouteComponentProps } from 'react-router';

interface IProps {
  communityTitle: string
}

interface PropsFromState {
  ui: UiState,
  posts: NormalizedObjects<PostState>
}

interface PropsFromDispatch {
  sortCommunityPosts: typeof initSortCommunityPosts, // sortCommunityPosts
  startSpinner: typeof startSpinner,
  initLoadMoreCommunityPosts: typeof initLoadMoreCommunityPosts // initLoadMoreCommunityPosts
}

type allProps = PropsFromState & PropsFromDispatch & IProps & RouteComponentProps<{ title: string }>;

interface IState {
  sortType: string
}

class Community extends Component<allProps, IState> {
  readonly state = {
    sortType: "popular"
  }

  componentDidMount() {
    if(this.props.ui.communityPosts.length === 0)
      this.props.sortCommunityPosts(this.state.sortType, this.props.match.params.title);
  }

  render() {
    return (
      <div>
        <Header isLoggedUser={this.props.ui.loggedUser === 0 ? false : true}></Header>
        <div className={styles.radioButtons}>
          <RadioGroup className={styles.radioButtons} onChange={this.onSortChange}>
            <FormControlLabel 
              value="popular"
              control={<Radio checked={this.state.sortType === "popular"}></Radio>}
              label="Popular">
            </FormControlLabel>
            <FormControlLabel 
              value="best"
              control={<Radio checked={this.state.sortType === "best"}></Radio>}
              label="Best">
            </FormControlLabel>
            <FormControlLabel
              value="new"
              control={<Radio checked={this.state.sortType === "new"}></Radio>}
              label="New">
            </FormControlLabel>
          </RadioGroup>
        </div>
        <div className={styles.postsContainer}>
          {this.props.ui.communityPosts.map(post => {
            return (
              <Post key={post}
                isOpened={this.props.ui.isOpenedSinglePost}
                postState={this.props.posts.byId[post]}
                cardWidthInPercentage="60%">
              </Post>
            )
          })}
        </div>
        <div className={styles.postsContainer}>
          <Button 
            className={styles.loadMoreButton} size="large" variant="outlined"
            onClick={this.loadMorePostsClick}>
            <ArrowDownwardIcon className={styles.loadMoreIcon} fontSize="inherit"/>
          </Button>
        </div>
      </div>
    );
  }

  onSortChange = (event: React.ChangeEvent<{}>, value: string) => {
    this.props.startSpinner();
    this.setState({...this.state, sortType: value});
    this.props.sortCommunityPosts(value, this.props.match.params.title);
  }

  loadMorePostsClick = () => {
    this.props.startSpinner();
    this.props.initLoadMoreCommunityPosts(
      this.props.ui.homePosts, this.state.sortType, this.props.match.params.title);
  }
}


const mapStateToProps = (rootReducer: any) => {
  return {
    ui: rootReducer.ui,
    posts: rootReducer.posts
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    sortCommunityPosts: (sortType: string, community: string) => 
      dispatch(initSortCommunityPosts(sortType, community)),
    startSpinner: () => dispatch(startSpinner()),
    initLoadMoreCommunityPosts: (posts: number[], category: string, community: string) => 
      dispatch(initLoadMoreCommunityPosts(posts, category, community))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Community);