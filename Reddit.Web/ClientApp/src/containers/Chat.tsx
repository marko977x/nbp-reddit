import { Button, Card, CardContent, List, ListItem, ListItemText, Paper, TextField, Typography, Divider, CssBaseline } from "@material-ui/core";
import React, { Component, ChangeEvent } from "react";
import { connect } from "react-redux";
import Header from "../components/header/Header";
import style from "./css/chat.module.css";
import { UiState } from "../store/ui/types";
import { NormalizedObjects } from "../store";
import { CommunityState } from "../store/community/types";
import { MessageState } from "../store/message/types";
import { UserState } from "../store/user/types";
import { initSendMessage, fetchMessagesAndSubscribe } from "../store/message/action";
import { Dispatch } from "redux";
import { startSpinner, selectChatCommunity } from "../store/ui/action";

interface PropsFromDispatch {
  initSendMessage: typeof initSendMessage,
  startSpinner: typeof startSpinner,
  selectChatCommunity: typeof selectChatCommunity,
  fetchMessages: typeof fetchMessagesAndSubscribe
}

interface PropsFromState {
  ui: UiState,
  communities: NormalizedObjects<CommunityState>,
  messages: NormalizedObjects<MessageState>,
  users: NormalizedObjects<UserState>
}

interface State {
  message: string,
  selected: number
}

type allProps = PropsFromDispatch & PropsFromState;

class Chat extends Component<allProps, State>{
  readonly state = {
    message: "",
    selected: 0
  }

  componentWillUpdate() {
    if(!this.props.messages.isLoaded && this.props.users.isLoaded) {
      this.props.fetchMessages();
    }
  }

  render() {
    return(
      <div>
        <Header isLoggedUser={this.props.ui.loggedUser !== 0}></Header>
        {this.renderChatPage()}
      </div>
    )
  }

  renderChatPage = () => {
    if(!this.props.communities.isLoaded || !this.props.users.isLoaded) 
      return(<div></div>);

    return(
    <div className={style.container}>
      <Paper className={style.paper}>
        <div className={style.communities}>
          <List>
            {this.props.users.byId[this.props.ui.loggedUser].communities.map(community => {
              return(
                <ListItem key={community} divider={true} button onClick={() => this.onCommunityClick(community)}
                  selected={community == this.props.ui.selectedChatCommunity}>
                  <ListItemText primary={this.props.communities.byId[community].title} />
                </ListItem>
              )
            })}
          </List>
        </div>
        {this.renderMessages()}
      </Paper>
    </div>)
  }

  renderMessages = () => {
    if(this.props.ui.selectedChatCommunity === 0) 
      return(
        <Card className={style.messageCard}>
          <CardContent className={style.content}>
            <Typography variant="h6">Reddit</Typography>
            <Typography variant="body2">Pick a community to start chatting</Typography>
          </CardContent>
        </Card>
      );

    if(this.props.messages.isLoaded) {
      return(
        <div className={style.messages}>
          <div className={style.messagesContainer}>
            <div className={style.messageContainer}>
              {this.props.communities.byId[this.props.ui.selectedChatCommunity].messages.map(message => {
                return(
                  <Card className={style.messageCard} key={message}>
                    <CardContent className={style.content}>
                      <Typography variant="h6">{this.props.users.byId[this.props.messages.byId[message].sender].username}</Typography>
                      <Typography variant="body2">{this.props.messages.byId[message].content}</Typography>
                    </CardContent>
                  </Card>
                )
              })}
            </div>
          </div>
          <div className={style.input}>
            <TextField id="outlined-full-width" label="Message" className={style.textField}
              style={{ margin: 8 }} placeholder="Enter your message"
              fullWidth margin="normal" InputLabelProps={{shrink: true}}
              variant="outlined" value={this.state.message} onChange={this.onContentChange}/>
            <Button className={style.button} color="primary" 
              variant="contained" onClick={this.onSendButtonClick}>
              Send
            </Button>
          </div>
        </div>);
    }
  }

  onContentChange = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({message: event.currentTarget.value});
  }

  onCommunityClick = (community: number) => {
    this.props.selectChatCommunity(community);
    this.setState({...this.state, selected: community});
  }

  onSendButtonClick = () => {
    if(this.state.message !== "") {
      this.props.startSpinner();
      this.props.initSendMessage({
        community: this.props.ui.selectedChatCommunity,
        content: this.state.message,
        id: 0,
        sender: this.props.ui.loggedUser
      });
      this.setState({...this.state, message: ""});
    }

  }

}

const mapStateToProps = (reducer: any) => {
  return {
    ui: reducer.ui,
    communities: reducer.communities,
    messages: reducer.messages,
    users: reducer.users
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    initSendMessage: (message: MessageState) => dispatch(initSendMessage(message)),
    startSpinner: () => dispatch(startSpinner()),
    selectChatCommunity: (community: number) => dispatch(selectChatCommunity(community)),
    fetchMessages: () => dispatch(fetchMessagesAndSubscribe())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);