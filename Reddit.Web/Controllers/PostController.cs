﻿using Microsoft.AspNetCore.Mvc;
using Reddit.Business.PostManagement;
using Reddit.Business.PostManagement.Input;
using Reddit.Business.ReduxLoaderManagement;
using Reddit.Domain.Interop;
using Reddit.Services.Repositories.Posts;
using System.Threading.Tasks;

namespace Reddit.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [Produces("application/json")]
    public class PostController : ControllerBase
    {
        private IPostManager _postManager;
        private IPostRepository _postRepository;
        private readonly IReduxLoaderManager _reduxLoaderManager;

        public PostController(
            IPostManager postManager,
            IPostRepository postRepository,
            IReduxLoaderManager reduxLoaderManager)
        {
            _postManager = postManager;
            _postRepository = postRepository;
            _reduxLoaderManager = reduxLoaderManager;
        }

        [HttpPost]
        public async Task<IActionResult> GetMorePosts([FromBody]MorePostsInput mpi)
        {
            Result<AppState> result;

            if (mpi.UserId == 0)
            {
                result = await _reduxLoaderManager
                    .LoadMorePosts(mpi.PostsIds, mpi.Category).ConfigureAwait(false);
            }
            else
            {
                result = await _reduxLoaderManager
                    .LoadMorePosts(mpi.PostsIds, mpi.Category, mpi.UserId).ConfigureAwait(false);
            }

            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetMorePostsOfCommunity([FromBody]MorePostsInput mpi)
        {
            Result<AppState> result = await _reduxLoaderManager.LoadMorePostsOfCommunity(
                mpi.PostsIds, mpi.Category, mpi.Community).ConfigureAwait(false);

            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddPost([FromBody]CreatePostInput input)
        {
            Result<long> result = await _postManager.AddPost(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddCommentToPost([FromBody]CreateCommentInput input)
        {
            Result<long> result = await _postManager.AddCommentToPost(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        //[HttpPost]
        //public async Task<IActionResult> DeletePost (long Id)
        //{
        //     _postRepository.Delete(Id);
        //    return Ok(new Result() { Success = true });
        //}

        [HttpPost]
        public async Task<IActionResult> LikePost(long postId, long userId, string communityTitle)
        {
            Result result = await _postManager.LikePost(postId, userId, communityTitle);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> DislikePost(long postId, long userId, string communityTitle)
        {
            Result result = await _postManager.DislikePost(postId, userId, communityTitle);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }
    }
}
